import copy
from typing import Callable, Dict, List, Optional, Tuple, Union

import numpy as np
import rootutils
import torch
import torch.nn as nn
import torch.nn.functional as F
from lightning.pytorch.core.mixins import HyperparametersMixin
from omegaconf import OmegaConf
from torch import Tensor

rootutils.setup_root(__file__, indicator=".project-root", pythonpath=True)

from architectures.backbones.hrnet import HRNet
from architectures.backbones.mobileone import reparameterize_model


class GaussianLoss(nn.Module):
    def __init__(
        self, custom: bool = True, reduction: str = "mean", constant: bool = False
    ) -> None:
        super().__init__()

        self.custom = custom
        self.gloss = nn.GaussianNLLLoss(full=constant, reduction=reduction, eps=1e-06)

    def forward(self, mean: Tensor, std: Tensor, gt: Tensor) -> Tensor:
        if self.custom:
            loss = torch.exp(-((mean - gt) ** 2.0) / std / 2.0) / torch.sqrt(
                2.0 * np.pi * std
            )
            loss = -torch.log(loss + 1e-9)
            loss = torch.mean(loss)
        else:
            loss = self.gloss(mean, gt, std)
        return loss


class ROIExtractor(nn.Module):
    def __init__(self, n_pts: int) -> None:
        super(ROIExtractor, self).__init__()
        self.n_pts = n_pts

    def forward(
        self, box_min: Tensor, box_scale: Tensor
    ) -> Tuple[Tensor, Tensor, Tensor]:
        box_min = torch.clamp(box_min - box_scale, 0.0, 1.0)
        box_min = torch.where(
            box_min > (1 - 2 * box_scale), (1 - 2 * box_scale), box_min
        )
        box_max = box_min + 2 * box_scale
        box = torch.cat((box_min, box_max), dim=2)
        box_len = box_max - box_min

        xs = F.interpolate(
            box[:, :, 0::2], size=self.n_pts, mode="linear", align_corners=True
        )
        ys = F.interpolate(
            box[:, :, 1::2], size=self.n_pts, mode="linear", align_corners=True
        )

        xs, ys = xs.unsqueeze(3).repeat_interleave(self.n_pts, dim=3), ys.unsqueeze(
            2
        ).repeat_interleave(self.n_pts, dim=2)

        mesh_grid = torch.stack([xs, ys], dim=-1)

        return mesh_grid, box_len, box_min


class Interpolation(nn.Module):
    def __init__(self) -> None:
        super(Interpolation, self).__init__()

    def _index_pts(
        self, anchor: Tensor, feature_maps: Tensor, feature_dim: List[int]
    ) -> Tensor:
        anchor_dim = anchor.size()

        index = anchor[:, :, 1] * feature_dim[2] + anchor[:, :, 0]

        batch_index = (
            torch.arange(0, feature_dim[0], dtype=index.dtype, device=index.device)
            * (feature_dim[2] * feature_dim[3])
        ).unsqueeze(1)

        index = (index + batch_index).flatten(0)

        out = torch.index_select(
            feature_maps.permute(1, 0, 2, 3).contiguous().flatten(1),
            1,
            index.int(),
        )
        out = out.view(feature_dim[1], feature_dim[0], anchor_dim[1])

        return out.permute(1, 2, 0).contiguous()

    def _interpolate(
        self, anchor: Tensor, feature_maps: Tensor, feature_dim: List[int]
    ) -> Tensor:
        anchor_lt = anchor.floor().to(anchor.dtype)  # .long()
        anchor_rb = anchor.ceil().to(anchor.dtype)  # .long()
        anchor_lb = torch.stack([anchor_lt[:, :, 0], anchor_rb[:, :, 1]], 2)
        anchor_rt = torch.stack([anchor_rb[:, :, 0], anchor_lt[:, :, 1]], 2)

        vals_lt = self._index_pts(
            anchor_lt.detach() if self.training else anchor_lt, feature_maps, feature_dim
        )
        vals_rb = self._index_pts(
            anchor_rb.detach() if self.training else anchor_rb, feature_maps, feature_dim
        )
        vals_lb = self._index_pts(
            anchor_lb.detach() if self.training else anchor_lb, feature_maps, feature_dim
        )
        vals_rt = self._index_pts(
            anchor_rt.detach() if self.training else anchor_rt, feature_maps, feature_dim
        )

        coords_offset_lt = anchor - anchor_lt.type(anchor.dtype)

        vals_t = vals_lt + (vals_rt - vals_lt) * coords_offset_lt[:, :, 0:1]
        vals_b = vals_lb + (vals_rb - vals_lb) * coords_offset_lt[:, :, 0:1]
        mapped_vals = vals_t + (vals_b - vals_t) * coords_offset_lt[:, :, 1:2]

        return mapped_vals

    def forward(self, anchor: Tensor, feature_maps: Tensor) -> Tensor:
        feature_dim = feature_maps.size()
        anchor = anchor * feature_dim[2] - 0.5
        anchor = torch.clamp(anchor, 0, feature_dim[2] - 1)
        anchor = self._interpolate(anchor, feature_maps, feature_dim)

        return anchor


class InherentRelation(nn.Module):
    def __init__(
        self,
        n_heads: int,
        embed_dim: int,
        linear_dim: int = 2048,
        activation: Callable = F.relu,
        dropout: float = 0.1,
        pre_norm: bool = False,
        return_cross_attn: bool = False,
    ) -> None:
        super(InherentRelation, self).__init__()
        self.pre_norm = pre_norm
        self.return_cross_attn = return_cross_attn

        self.activation = activation

        self.self_attn = nn.MultiheadAttention(embed_dim, n_heads, dropout=dropout)
        self.cross_attn = nn.MultiheadAttention(embed_dim, n_heads, dropout=dropout)

        self.linear_1 = nn.Linear(embed_dim, linear_dim)
        self.dropout_lin = nn.Dropout(dropout)
        self.linear_2 = nn.Linear(linear_dim, embed_dim)

        self.norm_1 = nn.LayerNorm(embed_dim)
        self.norm_2 = nn.LayerNorm(embed_dim)
        self.norm_3 = nn.LayerNorm(embed_dim)
        self.dropout_1 = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)
        self.dropout_3 = nn.Dropout(dropout)

    def _add_pos(self, x: Tensor, pos: Optional[Tensor]) -> Tensor:
        return x if pos is None else x + pos

    def forward(
        self,
        tgt: Tensor,
        memory: Tensor,
        tgt_mask: Optional[Tensor] = None,
        memory_mask: Optional[Tensor] = None,
        tgt_key_padding_mask: Optional[Tensor] = None,
        memory_key_padding_mask: Optional[Tensor] = None,
        pos: Optional[Tensor] = None,
        query_pos: Optional[Tensor] = None,
    ) -> Tuple[Tensor, Optional[Tensor]]:
        tgt_ = tgt
        if self.pre_norm:
            tgt = self.norm_1(tgt)

        q = k = self._add_pos(tgt, query_pos)

        tgt = self.self_attn(
            q,
            k,
            value=tgt,
            attn_mask=tgt_mask,
            key_padding_mask=tgt_key_padding_mask,
        )[0]

        tgt = tgt_ + self.dropout_1(tgt)

        if self.pre_norm:
            tgt_ = tgt
            tgt = self.norm_2(tgt)
        else:
            tgt = self.norm_1(tgt)
            tgt_ = tgt

        tgt, attn_weights = self.cross_attn(
            query=self._add_pos(tgt, query_pos),
            key=self._add_pos(memory, pos),
            value=memory,
            attn_mask=memory_mask,
            key_padding_mask=memory_key_padding_mask,
        )

        tgt = tgt_ + self.dropout_2(tgt)

        if self.pre_norm:
            tgt_ = tgt
            tgt = self.norm_3(tgt)
        else:
            tgt = self.norm_2(tgt)
            tgt_ = tgt

        tgt = self.linear_2(self.dropout_lin(self.activation(self.linear_1(tgt))))

        tgt = tgt_ + self.dropout_3(tgt)

        if not self.pre_norm:
            tgt = self.norm_3(tgt)

        if self.return_cross_attn:
            return tgt, attn_weights

        return tgt, None


class TransformerBlock(nn.Module):
    def __init__(
        self,
        inherent_relation: nn.Module,
        n_decoder: int,
        norm_layer: nn.Module = None,
        return_intermediate: bool = True,
        return_cross_attn: bool = False,
    ) -> None:
        super(TransformerBlock, self).__init__()
        self.inherent_relation = nn.ModuleList(
            [copy.deepcopy(inherent_relation) for i in range(n_decoder)]
        )
        self.norm_layer = norm_layer
        self.return_intermediate = return_intermediate
        self.return_cross_attn = return_cross_attn

    def forward(
        self,
        tgt: Tensor,
        memory: Tensor,
        tgt_mask: Optional[Tensor] = None,
        memory_mask: Optional[Tensor] = None,
        tgt_key_padding_mask: Optional[Tensor] = None,
        memory_key_padding_mask: Optional[Tensor] = None,
        pos: Optional[Tensor] = None,
        query_pos: Optional[Tensor] = None,
    ) -> Tuple[Tensor, Optional[Tensor]]:
        first_cross_attn: Optional[Tensor] = None
        intermediate_outputs = []
        for id_l, l in enumerate(self.inherent_relation):
            tgt, cross_attn = l(
                tgt,
                memory,
                tgt_mask=tgt_mask,
                memory_mask=memory_mask,
                tgt_key_padding_mask=tgt_key_padding_mask,
                memory_key_padding_mask=memory_key_padding_mask,
                pos=pos,
                query_pos=query_pos,
            )

            if id_l == 0:
                first_cross_attn = cross_attn

            if self.return_intermediate:
                intermediate_outputs.append(
                    self.norm_layer(tgt) if self.norm_layer is not None else tgt
                )

        if self.norm_layer is not None:
            tgt = self.norm_layer(tgt)
            if self.return_intermediate:
                intermediate_outputs.pop()
                intermediate_outputs.append(tgt)

        if self.return_intermediate:
            return torch.stack(intermediate_outputs), first_cross_attn

        return tgt, first_cross_attn


class InherentRelationTransformer(nn.Module):
    def __init__(
        self,
        n_pts: int,
        n_heads: int = 8,
        embed_dim: int = 256,
        n_decoder: int = 6,
        linear_dim: int = 1024,
        dropout: float = 0.1,
        activation: Callable = F.relu,
        pre_norm: bool = True,
        return_intermediate: bool = True,
        return_cross_attn: bool = False,
    ) -> None:
        super(InherentRelationTransformer, self).__init__()
        self.structure_encoding = nn.Parameter(torch.randn(1, n_pts, embed_dim))
        self.landmark_query = nn.Parameter(torch.randn(1, n_pts, embed_dim))

        self.transformer_block = TransformerBlock(
            InherentRelation(
                n_heads,
                embed_dim,
                linear_dim,
                activation,
                dropout,
                pre_norm,
                return_cross_attn,
            ),
            n_decoder,
            nn.LayerNorm(embed_dim),
            return_intermediate=return_intermediate,
            return_cross_attn=return_cross_attn,
        )

        for param in self.parameters():
            if param.dim() > 1:
                nn.init.xavier_uniform_(param)

    def forward(self, x: Tensor) -> Tuple[Tensor, Optional[Tensor]]:
        bs, n_feat, feat_len = x.size()

        x = x.permute(1, 0, 2)
        structure_encoding = self.structure_encoding.repeat(bs, 1, 1).permute(1, 0, 2)
        landmark_query = self.landmark_query.repeat(bs, 1, 1).permute(1, 0, 2)

        tgt = torch.zeros_like(landmark_query)
        tgt, cross_attn = self.transformer_block(
            tgt, x, pos=structure_encoding, query_pos=landmark_query
        )

        return tgt.permute(2, 0, 1, 3), cross_attn


class DSLPT(nn.Module, HyperparametersMixin):
    def __init__(
        self,
        im_size: int,
        n_pts: int,
        init_shape: str,
        n_sample: int = 7,
        n_heads: int = 8,
        embed_dim: int = 256,
        n_decoder: int = 6,
        linear_dim: int = 1024,
        dropout: float = 0.1,
        n_step: float = 3,
        patch_size_threshold: List[float] = [0.5, 0.7],
        backbone: nn.Module = None,
        return_intermediate: bool = True,
        return_cross_attn: bool = False,
        pretrained: str = None,
        legacy: bool = False,
    ) -> None:
        super(DSLPT, self).__init__()
        self.save_hyperparameters(ignore=["backbone"])

        self.n_step = n_step
        self.n_pts = n_pts
        self.n_sample = n_sample
        self.embed_dim = embed_dim
        self.n_decoder = n_decoder
        self.patch_size_threshold = OmegaConf.to_object(patch_size_threshold)

        init_shape = np.load(init_shape)["init_face"]

        init_shape = torch.from_numpy(init_shape / float(im_size))
        self.init_shape = init_shape.view(1, self.n_pts, 2).float()
        self.init_shape.requires_grad = False

        self.init_scale = torch.tensor([[[0.125, 0.125]]], dtype=torch.float32).repeat(
            1, self.n_pts, 1
        )
        self.init_scale.requires_grad = False

        self.roi = ROIExtractor(n_sample)
        self.interpolation = Interpolation()

        self.feature_layer = nn.Conv2d(
            embed_dim,
            embed_dim,
            kernel_size=n_sample,
            bias=True,
        )

        self.relation_layer = InherentRelationTransformer(
            n_pts,
            n_heads,
            embed_dim,
            n_decoder,
            linear_dim,
            dropout,
            return_intermediate=return_intermediate,
            return_cross_attn=return_cross_attn,
        )
        self.embedding_layer = nn.Linear(embed_dim, 2 * embed_dim)

        self.mean_layer = nn.Linear(2 * embed_dim, 2)
        self.std_layer = nn.Linear(2 * embed_dim, 2)

        for param in self.parameters():
            if param.dim() > 1:
                nn.init.xavier_uniform_(param)

        self.backbone = HRNet() if backbone is None else backbone
        self.hparams["backbone"] = self.backbone.hparams

        if pretrained is not None:
            self._load_model(pretrained, legacy)

    def _load_model(self, pretrained, legacy: bool = True):
        checkpoint = torch.load(pretrained, map_location=torch.device("cpu"))

        if legacy:  # backward compatibility
            state_map = {
                "feature_extractor.": "feature_layer.",
                # "feature_norm.": "norm_layer.",
                "Transformer.": "relation_layer.",
                ".Transformer_block.": ".transformer_block.",
                ".layers.": ".inherent_relation.",
                "embedding_layer.": "embedding_layer.",
                "out_mean_layer.": "mean_layer.",
                "out_std_layer.": "std_layer.",
                ".conv1.": ".conv_1.",
                ".conv2.": ".conv_2.",
                ".conv3.": ".conv_3.",
                ".bn1.": ".bn_1.",
                ".bn2.": ".bn_2.",
                ".bn3.": ".bn_3.",
                ".layer1.": ".layer_1.",
                ".layer2.": ".layer_2.",
                ".layer3.": ".layer_3.",
                ".layer4.": ".layer_4.",
                ".transition1.": ".transition_1.",
                ".transition2.": ".transition_2.",
                ".transition3.": ".transition_3.",
                ".stage2.": ".stage_2.",
                ".stage3.": ".stage_3.",
                ".stage4.": ".stage_4.",
                ".multihead_attn.": ".cross_attn.",
                ".linear1.": ".linear_1.",
                ".linear2.": ".linear_2.",
                ".norm1.": ".norm_1.",
                ".norm2.": ".norm_2.",
                ".norm3.": ".norm_3.",
                ".norm.": ".norm_layer.",
                "backbone.head.0.weight": "backbone.head.weight",
                "backbone.head.0.bias": "backbone.head.bias",
            }

            if "feature_norm.weight" in checkpoint:
                del checkpoint["feature_norm.weight"]
                del checkpoint["feature_norm.bias"]
        else:
            state_map = {
                "model.": "",
            }

            if "state_dict" in checkpoint:
                checkpoint = checkpoint["state_dict"]

        def update_key(key: str) -> str:
            for k, v in state_map.items():
                if k in key:
                    key = key.replace(k, v)
            return key

        checkpoint = {update_key(k): v for k, v in checkpoint.items()}

        self.load_state_dict(checkpoint)

    def forward(
        self,
        x: Tensor,
        n_step: Optional[int] = None,
        threshold: Optional[float] = None,
        return_dict: bool = False,
        init_face: Optional[List[Tensor]] = None,
    ) -> Union[
        Dict[str, Union[Tensor, List[Tensor]]],
        Tuple[Optional[Tensor], Optional[Tensor]],
    ]:
        n_step = self.n_step if n_step is None else n_step
        bs = x.size(0)
        feature_maps = self.backbone(x)

        if init_face is None:
            init_shape = self.init_shape.repeat(bs, 1, 1).to(x.device).to(x.dtype)
            init_scale = self.init_scale.repeat(bs, 1, 1).to(x.device).to(x.dtype)
        else:
            init_shape = init_face[0]
            init_scale = init_face[1]

        step_dict = {
            key: []
            for key in ["landmarks", "scale", "box", "distribution", "cross_attention"]
        }

        landmarks: Optional[Tensor] = None
        confidence: Optional[Tensor] = None

        for step in range(n_step):
            roi_anchor, box_size, box_min = self.roi(
                init_shape.detach() if self.training else init_shape, init_scale
            )
            roi_anchor = roi_anchor.view(
                bs,
                self.n_pts * self.n_sample * self.n_sample,
                2,
            )

            if feature_maps.__class__ is list:
                fm_list = []
                for fm in feature_maps:
                    roi_features = self.interpolation(
                        roi_anchor.detach() if self.training else roi_anchor,
                        fm,
                    )

                    roi_features = roi_features.view(
                        bs * self.n_pts,
                        self.n_sample,
                        self.n_sample,
                        fm.shape[1],
                    ).permute(0, 3, 2, 1)

                    fm_list.append(roi_features)

                roi_features = torch.cat(fm_list, 1)
            else:
                roi_features = self.interpolation(
                    roi_anchor.detach() if self.training else roi_anchor, feature_maps
                )

                roi_features = roi_features.view(
                    bs * self.n_pts,
                    self.n_sample,
                    self.n_sample,
                    self.embed_dim,
                ).permute(0, 3, 2, 1)

            relation_features = self.feature_layer(roi_features).view(
                bs, self.n_pts, self.embed_dim
            )

            offset, cross_attn = self.relation_layer(relation_features)
            offset = F.relu(self.embedding_layer(offset))
            offset_mean = self.mean_layer(offset)
            offset_std = self.std_layer(offset)
            offset_std = 1 / (1 + torch.exp(-offset_std))

            landmarks = box_min.unsqueeze(1) + box_size.unsqueeze(1) * offset_mean
            confidence = offset_std[:, -1, :, :]

            if step < n_step - 1:
                init_shape = landmarks[:, -1, :, :]
                init_scale = init_scale * torch.clamp(
                    torch.max(
                        offset_std[:, -1, :, :].detach() * 12
                        if self.training
                        else offset_std[:, -1, :, :] * 12,
                        dim=2,
                        keepdim=True,
                    )[0],
                    self.patch_size_threshold[0],
                    self.patch_size_threshold[1],
                )

            if return_dict:
                step_dict["landmarks"].append(landmarks[:, -1, :, :])
                step_dict["scale"].append(init_scale)
                step_dict["box"].append([box_min, box_size])
                step_dict["distribution"].append([offset_mean, offset_std])
                step_dict["cross_attention"].append(cross_attn)

            if threshold is not None and offset_std[-1, :, :].mean() < threshold:
                break

        if return_dict:
            return step_dict
        if landmarks is not None and confidence is not None:
            return landmarks, confidence
        else:
            return None, None
