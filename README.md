# face-analysis

## DSLPT
model.py is largely based on: https://github.com/Jiahao-UTS/DSLPT  
License: GPL v2

## MobileOne
mobileone.py is taken from: https://github.com/apple/ml-mobileone  
License: Apple
